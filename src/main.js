import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import { SchedulePlugin, Week } from '@syncfusion/ej2-vue-schedule';

Vue.config.productionTip = false

new Vue({
  vuetify,
  SchedulePlugin, Week,
  render: h => h(App)
}).$mount('#app')
